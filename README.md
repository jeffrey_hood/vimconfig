# vimconfig
## JHood VIM config


-----
## Vundle package manager [Need to install Vundle package manager]
```
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
:PackageInstall
```
-----

## Miscellaneous Command Notes

* Leader bound to ,
* <Leader>e expands to :e {directory of current file}/ (open in the current buffer)
* Use <Leader>n to toggle NERDTree
* :5,10Align => to align lines 5-10 on =>'s
* Command T - command-t (<D-t>) - Search for a file inside the current directory
* Surround - cs"' to change "foo bar" into 'foo bar'
* NERDCommenter - command-/ (<D-/>) to toggle comments
* SuperTab - <TAB> to tab complete
